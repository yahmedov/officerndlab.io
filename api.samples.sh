# Check availability and price for a specific booking (i.e. booking summary):
curl -X POST \
  https://app.officernd.com/api/v1/organizations/icowork/bookings/summery \
  -H 'authorization: Bearer your-token' \
  -H 'content-type: application/json' \
  -d '{
    "booking" : {
        "resourceId": "58e39a90b00701ab70f21e85",
        "start": {
            "dateTime": "2017-10-01T15:00:00Z"
        },
        "end": {
            "dateTime": "2017-10-01T16:00:00Z"
        },
        "extras": {
            "58b032d3afc0d61216e07d19": 1
        },
        "summary": "test1"
    }, "target": {
        "team": "56535edb3492382393f68e5d",
        "member": "56535fc13492382393f68e62"
    }
}'

# Create two bookings with a single requesst (will be rolled out tomorrow):
curl -X POST \
  https://app.officernd.com/api/v1/organizations/icowork/bookings \
  -H 'authorization: Bearer your-token' \
  -H 'content-type: application/json' \
  -d '[{
    "team": "56535edb3492382393f68e5d",
    "member": "56535fc13492382393f68e62"
    "resourceId": "58e39a90b00701ab70f21e85",
    "start": {
        "dateTime": "2017-10-01T15:00:00Z"
    },
    "end": {
        "dateTime": "2017-10-01T16:00:00Z"
    },
    "extras": {
        "58b032d3afc0d61216e07d19": 1
    },
    "summary": "test1"
}, {
    "team": "56535edb3492382393f68e5d",
    "member": "56535fc13492382393f68e62"
    "resourceId": "58e39a90b00701ab70f21e85",
    "start": {
        "dateTime": "2017-10-01T17:00:00Z"
    },
    "end": {
        "dateTime": "2017-10-01T18:00:00Z"
    },
    "extras": {
        "58b032d3afc0d61216e07d19": 5
    },
    "summary": "test2"
}]'

# Resources and resource rates.
# Get all meeting rooms:
curl -X GET \
  https://app.officernd.com/api/v1/organizations/icowork/resources?type=meeting_room

# As a result you'll receive a a list of resources, each of which has rate property. You can use the value of the rate property to get the actual rate (will be rolled out tomorrow):
curl -X GET \
  https://app.officernd.com/api/v1/organizations/icowork/rates/56d19207b93f4bf6389127ee

# or just get all the rates and map them to the resources
curl -X GET \
  https://app.officernd.com/api/v1/organizations/icowork/rates

# Members
# Create a member:
curl -X POST \
  https://app.officernd.com/api/v1/organizations/icowork/members \
  -H 'authorization: Bearer your-token' \
  -H 'content-type: application/json' \
  -d '[{
  "name": "John Doe",
  "email": "john@doe.com",
  "phone": "+442130172"
}]'