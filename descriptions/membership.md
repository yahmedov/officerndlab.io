  Membership objects allow you to assign recurring charges (price plans) to individual members or teams.
  The API allows you to create, delete, and update your memberships.
  You can retrieve individual memberships as well as a list of all your memberships.

  Here is the list of all properties the membership object has:

| Property      | Type              | Required | Description                                      |
| ------------- | ----------------- | -------- | ------------------------------------------------ |
| name          | string            | true     | The display name of the membership. Used to generate invoice line items as well |
| price         | number            | true     | The monthly price of the membership              |
| team          | [Team](#team)     | false    | Reference to the company (required if no member) |
| member        | [Member](#member) | false    | Reference to the company (required if no member) |
| office        | [Office](#office) | true     | A reference to the location the fee is issued for |
| plan          | [Plan](#plan)     | true     | A reference to the price plan assigned to the membership. It is used to determine the sales account when generating an invoice |
| isPersonal    | boolean           | false    | If true, the membership is is billed to the assigned member and not to the company |
| startDate     | date              | true     | The membership starting date                           |
| endDate       | date              | false    | The last date of membership. If not set the membership is open-ended |
| discount      | id                | false    | A reference to a discount definition                  |
| discountAmount| number            | false    | Manually granted discount for the specific membership |
| calculatedDiscountAmount| number | readonly | The actual discount amount comming from a discount defintion or from a manually granted discount |
| discountedPrice| number | readonly | The final membership unit price after granting the discount |
| locked        | boolean           | false    | If true, prevents editing the membership         |
