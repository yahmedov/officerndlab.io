Office objects represent the separate physical locations in the organization. They are used to separate resources and members in a single organization.

### Office

| Property      | Type    | Required | Description                                                        |
| ------------- | ------- | -------- | ------------------------------------------------------------------ |
| name          | string  | true     | The name of the plan in hand. It appears everywhere in the system. |
| country       | string  | false    | The country part of the location address.                          |
| state         | string  | false    | The state part of the location address.                            |
| city          | string  | false    | The city part of the location address.                             |
| address       | string  | false    | The address part of the location address.                          |
| zip           | string  | false    | The zip code part of the location address.                         |
| timezone      | string  | false    | The timezone the location is at.                                   |
| image         | string  | false    | The URL pointing to an image assigned to this location.            |
| isOpen        | boolean | false    | If true, the location is operational; if false or ommited the location is not working - either not open yet, or suspended for some other reason. |
| public      | boolean | false    | If true, the location is shown in the members portal; if false or ommited the location is not displayed in the members portal. |
