Authenticate member by given email. Returns the access token to login (impersonate) as a member.

The authentication token returned by this endpoint can be used to generate quick login links to the members portal. You just need to add access_token=&lt;token&gt; to the URL.

*Example:*

```
  https://icowork.officernd.com/calendar?access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU4NDZmMjhhZmY1ZDg4MTI2YWJjZmMzZiIsImlhdCI6MTQ4MTExNjQwOCwiZXhwIjoxNDgxMjg5MjA4fQ.ElceBwjTRBhu669SWkn_Za8VKk-cwomzmbEsNywPt5A
```
