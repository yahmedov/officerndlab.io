OfficeR&D public API is designed to let 3rd party applications communicate with OfficeR&D database.

The API is designed after the system entities - there is a separate resource for every entity and they can be listed, viewed by ID, added, updated by id and deleted by id.

Two APIs are available serving different purposes:

* ** Server to server ** - available at /{resource}. Usually, used for integrating server-based applications.
  * Authentication - token-based 
* ** Client to server ** - available at /user/{resource} - Usually, used for integrating Mobile apps, Member portals, etc.
  * Authentication - authenticated as a user/member. In order to call any of the User APIs, you need to provide a valid Authorization token, received by ** [/auth/signin](#auth) ** endpoint.