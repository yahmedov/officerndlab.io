Bookings are short-term reservation of resource. Bookings can reserve any resource, such as Meeting Room, Hot desk, Private Office, etc.

The API allows you to create, delete, and update bookings.

Here is the list of most properties the booking:

### Booking

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| start.dateTime | date    | true     | Start datetime of the booking in ISO date time format |
| end.dateTime  | date    | true     | End datetime of the booking in ISO date time format |
| timezone      | string  | readonly | The booking resource timezone |
| source        | string  | false    | The origin/source of the booking                |
| summary       | string  | false    | Booking title/summary                           |
| resourceId    | id      | true     | Reference to the booked resource                |
| plan          | id      | readonly | Reference to the resource rate used for the booking |
| team          | id      | false    | Reference to the company (required if no member) |
| member        | id      | false    | Reference to the member (required if no team)    |
| description   | string  | false    | Long description of the booking                  |
| tentative     | boolean | false    | True if the booking is not confirmed. Tentative booking are not charged. |
| free          | boolean | false    | True if the booking is marked as free. If create a booking marked as free, no fees will be added for it. |
| fees          | array | readonly | Array of items describing how the booking will be charged for. Described below as BookingFee. |
| extras        | object  | readonly | Dictionary with keys the IDs of the extras to add to the booking and values - the quantity of the extra |

### Booking Fee

| Property      | Type    | Required | Description                                               |
| ------------- | ------- | -------- | --------------------------------------------------------- |
| date          | date    | true     | The date of the occurrence the fee has been generated for |
| fee           | [Fee](#fees) | false | The actual charge generated for this occurrence.        |
| credits       | array   | true     | The credits used to do the booking                        |
| extraFees     | array([Fee](#fees)) | true | Array of fees generated for the extras.           |