Fee objects describe one-off charges or groups of one-off charges. They might or might not be connected to a specific plan, but must have their own name and price.

You can retrieve individual fee as well as query all the fees by location, company or member.

Here is the list of all properties the fee object has:

### Fee

| Property      | Type              | Required | Description                                                      |
| ------------- | ----------------- | -------- | ---------------------------------------------------------------- |
| name          | string            | true     | The name of the charge. It will appear in the invoice line item. |
| price         | number            | true     | The unit price of the item described by the fee.                 |
| quantity      | number            | false    | The quantity described by the one-off charge. If ommited, 1 is assumed. |
| date          | date              | true     | The date when the service has been sold. This date is used to determine in which invoice the fee should be included. |
| team          | [Team](#team)     | false    | Reference to the company (required if no member) |
| member        | [Member](#member) | false    | Reference to the company (required if no member) |
| office        | [Office](#office) | true     | A reference to the location the fee is issued for |
| plan          | [Plan](#plan)     | false    | A reference to the price plan assigned to the fee. It is used to determine the sales account when generating an invoice |
| refundable    | boolean           | false    | If true, the one-off charge is counted as deposit and can be refuded later on. |
| billInAdvance | boolean           | false    | If true, the one-off charge is is billed in advance (respecting its date). By default one-off charges are billed in arears. |
| isPersonal    | boolean           | false    | If true, the one-off charge is is billed to the assigned member and not to the company. |