Many objects contain the Id of a related object in their response properties. For example, a Visit may has an associated Visitor Id. Those objects can be expanded inline with the $populate request parameter.

Objects that can be expanded are noted in this documentation. This parameter is available on all GET API requests. This option work for both object lists or requesting single object by id.
