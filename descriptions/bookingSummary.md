Validate a booking and returns if the booking is valid and any charges, fees, or credits will be used.

### Booking Summary

| Property      | Type                                                 | Required | Description                                     |
| ------------- | ---------------------------------------------------- | -------- | ----------------------------------------------- |
| target        | [Booking Target](#booking-target)                    | false    | Object, describing the member and company, which are about to create a booking. |
| booking       | [Booking](#booking)                                  | false | The booking to be validated if possible to be created without the team and member properties (required if `bookings` is not passed). |

### Booking Target

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| team          | id      | false    | The ID of the team in hand                      |
| member        | id      | false    | The ID of the member in hand                    |
