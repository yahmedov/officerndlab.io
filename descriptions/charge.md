Charge object describes a single payment associated with an invoice. Charges can be either created manually, or done via a payment gateway.

Here is the structure of the charge object

### Charge

| Property      | Type    | Required | Description                                           |
| ------------- | ------- | -------- | ----------------------------------------------------- |
| amount        | number  | true     | The amount of the transaction.                        |
| date          | date    | true     | The exact date of the transaction.                    |
| status        | enum    | true     | The status of the transaction. One of the following *success*, *pending*, *fail*. |
| reference     | string  | true     | Human readable description of the transaction.        |
| source        | string  | false    | The system the charge originated at. OfficeR&D by default |
| account       | string  | false    | The name of the account where money are depositet at. |
| integration   | object  | false    | Present if charge is collected via payment gateway. Its property providerChargeId is set to the gateway charge ID |
| createdAt     | date    | readonly | The date when the charge has been created at.         |
| createdBy     | id      | readonly | The user that created the charge.                     |
| payment       | id      | readonly | The invoice or credit note the charge is associated with. |