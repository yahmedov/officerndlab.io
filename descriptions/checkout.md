Checkout and checkout summary endpoints allow you to create a new order or get the preview of an order. Depending on the organization settings and the current member status they may create an invoice, charge the member and send an invoice automatically.

Here is the list of all properties the checkout request object has:

### Checkout Request

| Property      | Type    | Required | Description                         |
| ------------- | ------- | -------- |------------------------------------ |
| plans         | [[PlanRequest](#plan-request)]  | false     | A list of plan request definitions to be added to the order. |
| fees          | [[FeeRequest](#fee-request)]  | false     | A list of fee request definitions to be added to the order. |
| discountCodes | [string] | false   | 0 or 1 discount codes to be applied. |


### Plan Request

| Property      | Type    | Required | Description                                  |
| ------------- | ------- | -------- |--------------------------------------------- |
| plan          | id      | true     | The ID of the membership plan to be ordered. |
| startDate     | date    | true     | The start date of the ordered membership.    |
| endDate       | date    | false    | The end date of the ordered membership.      |


### Fee Request

| Property      | Type    | Required | Description                                  |
| ------------- | ------- | -------- |--------------------------------------------- |
| plan          | id      | true     | The ID of the membership plan to be ordered. |
| date          | date    | true     | The date the one-off is ordered for.         |