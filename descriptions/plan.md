Plan objects (or price plans) allow you to create one-off or recurring named charges.

You can retrieve individual plans as well as a list of all your plans.

### Plan

| Property      | Type    | Required | Description                                                        |
| ------------- | ------- | -------- | ------------------------------------------------------------------ |
| name          | string  | true     | The name of the plan in hand. It appears everywhere in the system. |
| description   | string  | false    | Text describing the plan. It will appear when users need to choose among different plans. |
| price         | number  | true     | The default unit price for the plan, depending if it is recurring plan or a one-off plan. |
| locations     | [id]    | true     | List of all locations where the plan is present. If empty, all locations are assumed. |
| type          | enum    | true     | The type of the plan. It could be office, desk, hotdesk or service. |
| intervalLength | enum    | true     | The plan interval. It could be month or once. |
| code          | string  | false    | External identified for the plan. Usually used for accounting purposes. |

*NOTE: Right now the plans API returns both rates and plans, so you should ignore all items with **isRate** property set to **true***