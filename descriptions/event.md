Event objects describe community events added to OfficeRnD.

You can retrieve individual event as well as query all the events by location, company or member.

Here is the list of the event object properties:

### Event

| Property      | Type    | Required | Description                                                      |
| ------------- | ------- | -------- | ---------------------------------------------------------------- |
| start         | date    | true     | The start date and time of the event. Must be a ISO-formatted in UTC. |
| end           | date    | true     | The end date and time of the event.  Must be a ISO-formatted in UTC. |
| office        | id      | true     | A reference to the location the invoiced is issued by. |
| team          | id      | false    | A reference to the company that organizes the event. |
| member        | id      | false    | A reference to the member that organizes the event. |
| title         | string  | false    | A brief title of the event. |
| description   | string  | false    | A detailed description of the event. |
| where         | string  | false    | Additional information for the location of the event. |
| timezone      | string  | false    | The timezone of the event |
| image         | string  | false    | A url to an image for the event. |
| limit         | number  | false    | The number of participants that this event will host. |