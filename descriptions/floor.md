Represents floor data. Floors are used to clasify different resources from the same location. It may contain floorplan data as well.

### Floor

| Property      | Type        | Required | Description                              |
| ------------- | ----------- | -------- | ---------------------------------------- |
| name          | string      | true     | The name of the floorplan - usually the floor number. |
| floor         | string      | false    | Optional floor number.                   |
| isOpen        | boolean     | false    | If true, the location is operational; if false or ommited the location is not working - either not open yet, or suspended for some other reason. |
| office        | [Office](#office)| true| A reference to the location the floor is part of |
| description   | string      | false    | Descriptio for the floor                |
| area          | number      | false    | The floor are of this part of the building. |
| targetRevenue | number      | false    | The target revenue for the specified floor |