Team objects allow you to keep your companies' data in single place, track memberships, invoices, charges, fees, and etc.

Everything that is associated with the same customer.
The API allows you to create, delete, and update your companies / teams.
You can retrieve individual teams as well as a list of all your teams.

Here is the list of all properties the team object has:

### Team

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| name          | string  | true     | The full name of the member                     |
| email         | string  | true     | The email address of the member                 |
| twitterHandle | string  | false    | The twitter handle of the member                |
| office        | id      | false    | Reference to the location the member belongs to. It will default to the only location if left empty |
| paymentDetails | array  | readonly | Array containing all the names of the payment details added to the member account |
