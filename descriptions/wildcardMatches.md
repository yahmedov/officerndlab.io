For some string properties you can use wildcard matches if you want to find a group of entites. If you want to do wildcard search you can use **$sw**, **$ew**, **$cs** for starts with, ends with and contains matches. If you need to perform case insensitive search, just use **$swi**, **$ewi** and **$csi**.

Now, if you want to find all members with name starting with **"john"** no matter the case you can pass **name.$swi=john**.

Please notice that as these are query parameters their values need to be URL encoded.