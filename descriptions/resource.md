Resource objects describe all type of bookable (short term bookings or long term assignments) resources in the system.

Resource types can be extended and modified, but the default ones are:

 * meeting rooms - meeting_room
 * private offices - team_room
 * private office desk - desk_tr
 * dedicated desks - desk
 * hot desks - hotdesk

You can retrieve individual resources as well as a list of all your resources or all of a specific type.

### Resource

| Property      | Type    | Required | Description                                               |
| ------------- | ------- | -------- | --------------------------------------------------------- |
| name          | string  | true     | The name of the resource in hand.                         |
| rate          | [Rate](#rates) | false | The resource rate used for pricing bookings for this particualr resource. |
| office        | [Office](#office)| true     | The location the resource is assigned to                  |
| room          | [Floor](#floor) | true     | The floorplan the resource is assigned to                 |
| type          | enum    | true     | The type of the resource. One of types listed above.      |