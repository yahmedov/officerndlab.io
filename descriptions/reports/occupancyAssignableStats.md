Generates report containing occupancy statistics for all resource types, divided by periods as specified by the time period filter.

If not grouped, the report is an array of type [Interval Occupancy Stats](#interval-occupancy-stats).

### Interval Occupancy Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| date          | date      | The start date of the interval the stats are about (i.e. the group key).                            |
| total         | object    | The sum/avg of occupancy stats for all resource types. The value of this field is of type [Occupancy Stats](#occupancy-stats). |
| _* (resource type)_ | object | The occpancy stats for each resource type. The value of this field is of type [Extended Occupancy Stats](#extended-occupancy-stats). |

### Occupancy Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| count         | number    | The count of the specified resource type.                                                           |
| units         | number    | The total units count of the specified type. This property is missing for single resources like desks and contain the count of the sub resources for resources like private offices. |
| occupied      | number    | The count of the occupied units of the specified resource type. For single resources this is the count of the resources occupied. |
| occupancy     | number    | The occupancy rate for the specified resource type calculated based on units (i.e. occupied / units). For single resources this is the based on the resources count instead. |

### Extended Occupancy Stats

All properties from [Occupancy Stats](#occupancy-stats) plus

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| statuses      | [object]  | Resources count grouped by their status. The value of each item is of type [Status Group Stats](#status-group-stats). |


### Status Group Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| status        | string    | The status of the group.                                                                            |
| count         | number    | The count of resources of the specified type and status for the specific date.                      |
| units         | number    | The total units count of the specified type and status for the specific date.                       |

If the report is grouped (i.e. the $group query string parameter is passed), the report is an array of type [Group Occupancy Stats](#group-occupancy-stats).

### Group Occupancy Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| room          | ID        | Reference to the floorplan this group is for. Only present when grouped by *room*.                  |
| office        | ID        | Reference to the location this group is for. Only present when grouped by *office*.                 |
| stats         | object    | The stats for the specified group. The value of this field is an array of type [Interval Occupancy Stats](#interval-occupancy-stats). |