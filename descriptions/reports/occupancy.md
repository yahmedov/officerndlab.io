This set of reports provides aggregated data regarding the resources and members of the space. As this data is related to a specific time frame, these APIs allow quering data for different time ranges and divided by different time periods.

To set this time filter set the following query string parameters:

* from - start date of the period in ISO date format (i.e. 2017-10-01)
* to - end date of the period in ISO date format (i.e. 2017-11-31)
* interval - length of the time period the report is divided by (one of 'day', 'week', 'month')