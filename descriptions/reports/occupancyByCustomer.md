Generates a report for each customer, containing information about the resource the customer is using in the space in the specified time period.

If not grouped, the report is an array of type [By Customer Occupancy Stats](#by-customer-occupancy-stats). If grouped by office or floor, the result is an array of type [Grouped By Customer Occupancy Stats](#grouped-by-customer-occupancy-stats).
### By Customer Occupancy Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| team          | object    | The **_id** and **name** of the company the report entry is for (i.e. the group key).               |
| member        | object    | The **_id** and **name** of the company the report entry is for (i.e. the group key).               |
| stats         | [object]  | List of the report data divided by intervals. The value of this field is of type [Customer Occupancy Stats](#customer-occupancy-stats). |

### Customer Occupancy Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| date          | date      | The start date of the interval the stats are about (i.e. the subgroup key).                         |
| total         | object    | The sum of all occupied resource used by the customer. The value of this field is of type [Resource Type Stats](#resource-type-stats). |
| _* (resource type)_ | object | The count of resources used by the customer. The value of this field is of type [Resource Type Stats](#resource-type-stats). The name of the field could be one of desk, hotdesk, team_room, or any other resource type. |

### Resource Type Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| count         | number    | The count of resources of the specified type being used by the customer.  |
| units         | number    | The total units count of the specified type being used by the customer. This property is missing for single resources like desks and contain the count of the sub resources for resources like private offices. |

### Grouped By Customer Occupancy Stats

| Property      | Type      | Description                                                                                         |
| ------------- | --------- | --------------------------------------------------------------------------------------------------- |
| room          | ID        | Reference to the floorplan this group is for. Only present when grouped by *room*.                  |
| office        | ID        | Reference to the location this group is for. Only present when grouped by *office*.                 |
| byCustomer    | [object]  | The byCustomer report for the specified group. The value of this field is an array of type [By Customer Occupancy Stats](#by-customer-occupancy-stats). |