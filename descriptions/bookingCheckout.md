Booking checkout endpoint is used to automatically pick resources for booking based on a booking request. It can be used when it doesn't matter which resource should be booked, but it is more important certain criterias to be matched.

### Booking Request

| Property      | Type    | Required | Description                                       |
| ------------- | ------- | -------- | ------------------------------------------------- |
| start         | date    | true     | Start datetime of the booking request.            |
| end           | date    | true     | End datetime of the booking request.              |
| team          | id      | false    | Reference to the company (required if no member). |
| member        | id      | false    | Reference to the member (required if no team).    |
| extras        | object  | false    | Dictionary with keys the IDs of the extras to add to the booking and values - the quantity of the extra. |
| source        | string  | false    | The origin/source of the bookings.                |
| summary       | string  | false    | Booking title/summary.                            |
| description   | string  | false    | Long description of the booking.                  |
| tentative     | boolean | false    | True if the booking is not confirmed. Tentative booking are not charged. |
| free          | boolean | false    | True if the booking is marked as free. If create a booking marked as free, no fees will be added for it. |
| size          | number* | false    | The resource size to used.                        |
| count         | number  | true     | The count of bookings of this type to be made.    |
| rate          | id      | false    | The resource rate to be used to lookup available resources. |
| resourceType  | id      | false    | The resource type to be used to lookup available resources. |

number*: _You can use a query instead of a value, for example `{ size: { $gte: 2 } }`, meaning the size must be at least 2_