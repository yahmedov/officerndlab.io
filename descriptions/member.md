Member objects allow you to keep your member's data in a single place, track memberships, invoices, charges, fees, and etc. Everything that is associated with the same customer.

The API allows you to create, delete, and update your members. You can retrieve individual members as well as a list of all your members.

Here is the list of all properties the member object has:

### Member

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| name          | string  | true     | The full name of the member                     |
| email         | string  | true     | The email address of the member                 |
| phone         | string  | false    | The primary phone number of the member          |
| twitterHandle | string  | false    | The twitter handle of the member                |
| team          | id      | false    | Reference to the team the member belongs to     |
| contactPerson | boolean |          | True if the member is a contact persion in its company |
| billingPerson | boolean |          | True if the member is the billing person of its company and recieves all the invoices |
| office        | id      | false    | Reference to the location the member belongs to. It will default to the only location if left empty |
| paymentDetails | array  | readonly | Array containing all the names of the payment details added to the member account |
