OfficeR&D Hosted Payment Gateway extensibility allows for Developers to implement payment gateway integration that is not provided out-of-the-box.

## Configure your hosted payment gateway
To start developing and testing your hosted payment gateway, enable and configure it inside the OfficeR&D admin console, following these steps:

1. Login to the OfficeR&D admin console as an `Owner` and navigate to **Settings** > **Integartions**.
2. Locate the **Hosted Payment Gateway** under the Payment Gateways section.
3. Click the **Activate** link and then **Configure**


A dialog is shown that allows you to input and get all necessary information for the integration to work.
* The **Name** field is used to define the name of the integration. If you input **Cool Payments**, the customer will see that as **Pay with Cool Payments**
* The **Secret** field is read-only. Its value is initially hidden and you can retrieve it by hitting the _"eye"_ button next to it. You must use the value of this field to verify requests and sign your responses. This secret should be used only on your servers and should **never** be shown, served or shared with the end user.
* The **Gateway URL** should be a fully formed url i.e. - `https://your.integration.url/path`. You can put parameter placeholders that will be populated when OfficeR&D initiates the customer redirect request. The placeholders that you can use are as follows: `{{ amount }}`, `{{ reference }}`, `{{ transactionId }}`, `{{ signature }}`, `{{ redirectUrl }}`.
  * Example: `https://pay.cool-payments.com/hosted?amount={{ amount }}&ref={{ reference }}&tId={{ transactionId }}&signature={{ signature }}&redirectUrl={{ redirectUrl }}`

## How it works 
1. The customer chooses to pay with the hosted payment gateway, configured in OfficeR&D.
2. OfficeR&D redirects the customer to your gateway's URL, submitting the correct Request parameters. Your gateway must verify the signature parameter value and present a hosted payments page to the customer.
3. The customer payment is processed on the hosted payment page that you provide.
4. Upon completing the payment, customers should be redirected back to the redirectUrl with all required Response parameters in the query string. This includes the signature. If the payment was successful, the status parameter should have a `success` value. Otherwise the status should be - `fail`.
5. OfficeR&D accounts for the payment to the respective invoice and shows a success or failure message the customer.

## Developing your hosted payment gateway integration
OfficeR&D enables you to extend the system so that you can process payments using a custom-developed payment page. OfficeR&D checkout will redirect the customer to the configured payment gateway page. The page should be able to handle predefined request parameter values. After the customers complete the payment, they will be redirected back to OfficeR&D, with predefined response parameter values.
 
It is the responsibility of the payment gateway to build the page that handles the payment requests.

### Request and Response signing mechanism

All requests and responses must be signed and verified using [HMAC-SHA256](https://en.wikipedia.org/wiki/Hash-based_message_authentication_code). The signing should use the following rules:

* For `key` you must use the value if the **Secret** field defined in your integration configuration page.

* The value of `message` must be a string comprised of the values of all key parameters concatenated by the pipe symbol (**'|'**) as a separator.
  * The key parameters for the request are `amount`, `reference` and `transactionId`. 
      
      **Example:** If the request amount is 12.34, it has a reference **INV-001** and a transactionId - **6f47c19b-eb9d-4ee5-a144-19f7b9263183**, then the message should be the following string:

    `12.34|INV-001|6f47c19b-eb9d-4ee5-a144-19f7b9263183`
  * The key parameters for the response are `status` and `transactionId`.
      
      **Example:** If the response status is success and its transactionId is 6f47c19b-eb9d-4ee5-a144-19f7b9263183, then the message should be the following string:

  
    `success|6f47c19b-eb9d-4ee5-a144-19f7b9263183`

* Resulting codes must be hex-encoded and passed as value of signature parameter. Make sure to use case-insensitive comparison when verifying the provided signature values.

Assuming your HMAC key is "11111111-1111-1111-1111-111111111111", the signatures of the example messages should be:
  *  `12.34|INV-001|6f47c19b-eb9d-4ee5-a144-19f7b9263183` - `617b0b7d29e57db460208904f0e64c11092872d86faf581598c3ba9b8b0e00ae`
  *  `success|6f47c19b-eb9d-4ee5-a144-19f7b9263183` - `2c6d0e09f76011cebf9073a4fa08d752e5c5c440e750c6ee1f8ad5d5cce70bad`



### Request parameters
| Name          | Type    | Required | Description                                                                  |
| ------------- | ------- | -------- | -----------------------------------------------------------------------------|
| amount        | number  | true     | The amount that needs to be charged.                                         |
| reference     | string  | true     | A reference string that clarifies what the charge is for (i.e. INV-001).     |
| transactionId | string  | true     | A unique identifier that references the transaction.                         |
| signature     | string  | true     | A signature generated from the amount, reference and transactionId.          |
| redirectUrl   | string  | true     | A url to redirect back when charge is processed or canceled.                 |
### Response parameters 

| Name          | Type    | Required | Description                                                                  |
| ------------- | ------- | -------- | -----------------------------------------------------------------------------|
| status        | string  | true     | The status of the transaction - values should be either `success` or `fail`. |
| transactionId | string  | true     | The unique identifier that was passed in the initial request.                |
| signature     | string  | true     | A signature generated from the status and transactionId.                     |
| error         | string  | false    | An error message with more details about the reason of the failure.          |
