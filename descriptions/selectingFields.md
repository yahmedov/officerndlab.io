Often you don't need all the fields of a given object and you just transfer them without ever using them. You can optimize such quieries by passing the $select request parameter.

This parameter is available on all GET API requests. This option work for both object lists or requesting single object by id.
