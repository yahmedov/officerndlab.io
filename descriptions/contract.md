Contract objects allow you to associate contract data to your companies. Contracts can contain terms for both Services and Private offices.
  The API allows you to create, delete, and update contracts.
  You can retrieve individual contracts as well as a list of all your contracts.