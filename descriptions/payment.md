Payment objects describe invoices, credit notes (credit memos), charges and credit allocations. They might contain information about any external links the invoice has.

You can retrieve individual invoices/credit notes as well as query all the items.

Here is the list of all properties the payment object has:

### Payment

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| number        | string  | true     | The document number for the invoice / credit memo. |
| documentType  | enum    | false    | The type of the document. It could be either *invoice* or *creditNote*. If ommited, *invoice* is assumed. |
| date          | date    | true     | The issue date of the document.                  |
| dueDate       | date    | true     | The due date of the document.                    |
| team          | id      | false    | A reference to the company the invoice is issued for. |
| member        | id      | false    | A reference to the member or individual the invoice is issued for. |
| office        | id      | true     | A reference to the location the invoiced is issued by. |
| reference     | string  | false    | Additional data describing the invoice. It could be any string value. |
| taxType       | enum    | true     | The type of the tax calculation for the invoice. It could be *excluded*, *included* or *noTax*. |
| allocations   | array   | true     | A lost of credit allocations. Each allocation has amount and target. |
| invoiceLines  | array   | true     | A lost of invoice line items. |
| vatAmounts    | array   | true     | A lost of invoice vat amounts. |
| subtotal      | number  | true     | The total of the invoice / credit note before tax. |
| amount        | number  | true     | The total of the invoice / credit note after tax and discount. |
| payableAmount | number  | true     | Total amount due for this invoice. Voided and paid invoices has *payableAmount* of 0. |

### Credit Allocation

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| target        | id      | true     | If payment is an invoice, a reference to the credit note, sourcing the creidt. If payment is a credit note, a reference to the invoice the credit is allocated to. |
| amount        | number  | true    | The allocated amount. |

### Line Item

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| description   | string  | true     | Description of the line item.                   |
| account       | id      | true     | A reference to the account the line item is allocated to. |
| vatPercent    | number  | true     | The tax percent applied to the line item. |
| vatAmount     | number  | true     | The tax amount applied to the line item. |
| quantity      | number  | true     | The quantity of items described by the line item. |
| unitPrice     | number  | true     | The unit price as described in the system. |
| discountAmount| number  | true     | The calculated discount amount based on the discount set in the invoice. |
| baseUnitPrice | number  | true     | The calculated unit price without tax. |
| baseTotal     | number  | true     | The total wihtout tax. |
| total         | number  | true     | The total for the line item (including tax). |

### Vat Amount

| Property      | Type    | Required | Description                                     |
| ------------- | ------- | -------- | ----------------------------------------------- |
| percent       | number  | true     | The tax percent for the group.                  |
| total         | number  | true     | The tax total for the group.                    |
| taxRate       | id      | true     | A reference to the taxRate the group is for.    |