module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-raml2html');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        raml2html: {
            options: {
                mainTemplate: 'template.nunjucks',
                templatesPath: './theme',
                standalone: true
                // Default template is found here:
                // https://github.com/raml2html/raml2html/tree/master/lib
            },
            raml: {
                files: {
                    'public/index.html': 'officernd.raml'
                }
            }
        },
        watch: {
          raml2html: {
            files: ['**/*.raml', '**/*.md', 'theme/*'],
            tasks: ['raml2html'],
            options: {
              spawn: false,
            }
          }
        }
    });

    grunt.registerTask('default', ['raml2html:raml']);
};
